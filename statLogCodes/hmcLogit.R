rm(list=ls())

library(numDeriv)

set.seed(1)

data = read.table('satTrn.txt', header = FALSE)

X = as.matrix(data[, 1:36])
X = scale(X, center=TRUE, scale=TRUE)
x =  cbind(1, X)
n = dim(x)[1]
pTrain = dim(x)[2]

y = rep(0, n)	
y[data[, 37]==2] <- 1

nIter = 200000
burnIn = 1000

leapFrog = 20
epsilon= 0.075


sigma = 10

hmcLogit <- function(x, y, n.iter, epsilon, L){

n.obs = dim(x)[1]	
n.param = dim(x)[2]	

post.samp <- matrix(NA, n.iter, n.param)

acpt.ind <- 1
current.q <- q.hat
current.U <- U(x, y, current.q, sigma)
current.g = grad.U(x, y, current.q, sigma)

post.samp[1, ] <- current.q
for (i in 2:n.iter){
	samp <- HMC(x, y, current.q, current.U, current.g, sigma, n.param, epsilon, L)
	current.q <- samp$q
	current.U <- samp$U
	current.g <- samp$g
	post.samp[i, ] <- current.q
	acpt.ind[i] <- samp$Ind
	
	}
return(list(samp=post.samp, acpt.ind=acpt.ind))
}

HMC <- function(x, y, current.q, current.U, current.g, sigma, n.param, e, L){
	
e <- runif(1, 0.95*e, 1.05*e)	

q = current.q	
p = rnorm(n.param)
g = current.g

current.p = p
current.K = sum(current.p^2)/2
current.H = current.U+current.K

for(leap in 1:L){
	p = p - e*g/2
    q = q + e*p
    g = grad.U(x, y, q, sigma)
    p = p - e*g/2
}
proposed.U = U(x, y, q, sigma)
proposed.K = sum(p^2)/2 
proposed.H = proposed.U+proposed.K

acceptProb = min(1, exp(current.H - proposed.H))
if(runif(1)<acceptProb){
    return (list(q = q, U = proposed.U, g = g, Ind = 1))  # accept
}else{
    return (list(q = current.q, U = current.U, g = current.g, Ind = 0))  # reject
}
}


U = function(x, y, beta, sigma){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

E = -(logLike + logPrior)

}

grad.U = function(x, y, beta, sigma){

eta = x%*%beta;

dLogLike = t(x)%*%(y - exp(eta - log(1 + exp(eta)) ))
dLogPrior =  -(beta) / (sigma^2)
 
g = -(dLogLike + dLogPrior);


}


ac.time <- function(x, q, m){

n = length(x)

s2 = var(x)

batch.mean = NULL
for(i in 1:q){
	
	batch.mean[i] <- mean(x[(((i-1)*m)+1):(i*m)])
	
	}	

	return(m*var(batch.mean)/s2)
	
}



get.map = function(beta){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

E = -(logLike + logPrior)

}



n.param = dim(x)[2]

q.hat <- optim(rnorm(n.param, 0, .2), get.map, method = "BFGS")$par


start.time = proc.time()
res <- hmcLogit(x, y, nIter, epsilon, leapFrog)
end.time = proc.time()
cpu.time = end.time[1] - start.time[1]

samp = res$samp
save(samp, file='hmcLogit.Rdata')

get.logLike = function(beta){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

}

acp = round(mean(res$acpt.ind), 2)
print(paste('Acceptance rate: ', acp))

CPU = round(cpu.time/nIter, 4)

print(paste('CPU time/iter', CPU))


logLike <- NULL
for(i in 1:dim(samp)[1]){

	logLike[i] <- get.logLike(samp[i, ])	
	
}


act.batch = NULL
batch.size = 1000
n.batch = floor (nIter/batch.size)

for(i in 1:dim(samp)[2]){
	
	act.batch[i] = ac.time(samp[, i], n.batch, batch.size)	
	
}
act = round(max(act.batch), 2)

print(paste('Batch autocorrelation time: ', act))

act.loglike = round(ac.time(logLike, n.batch, batch.size), 2)

print(paste('Batch autocorrelation time-- logLike: ', act.loglike))

print(paste('Batch autocorrelation time X s: ', act*CPU))

print(paste('Batch autocorrelation time X L: ', act*leapFrog))

print(paste('Batch autocorrelation time X acpRate: ', act*acp))
