Split HMC is a Markov chain Monte Carlo (MCMC) algorithm for 
sampling probability distributions.
More specifically, it is an improved version of Hamiltonian Monte Carlo (HMC).
It speeds up HMC in a way that allows much of the movement around the state space 
to be done at low computational cost.
This is achieved by splitting the Hamiltonian into a major part 
that can be analytically/efficiently handled and a minor residual part. 
The major dynamics is solved with larger steps 
while the minor dynamics is simulated with smaller steps.

This repo contains R codes for examples in the following paper:

Babak Shahbaba, Shiwei Lan, Wesley O. Johnson, Radford M. Neal
Split Hamiltonian Monte Carlo
Statistics and Computing, Volume 24, Issue 3, Pages 339-349, 2014
http://link.springer.com/article/10.1007%2Fs11222-012-9373-1

CopyRight: Babak Shahbaba

Please cite the reference when using the codes, Thanks!

Shiwei Lan
9-18-2016