rm(list=ls())

library(mvtnorm)
#library(coda)

set.seed(1)

# This is how gisette.Rdata is genereated:

#X = read.table('gisette.dat', header=FALSE)
#X <- princomp(X)$score[, 1:100]
#x =  cbind(1, X)
#y = read.table('gisetteLab.dat', header=FALSE)
#y[y==-1] = 0
#y[y==1] = 1

#save(x, y, file='gisette.Rdata')

# This data set includes covariates x and response variable y
load('gisette.Rdata')


n = dim(x)[1]
pTrain = dim(x)[2]
	

nIter = 200000
#burnIn = 20

leapFrog = 14
epsilon= (20*0.05)/leapFrog

sigma = 10

hmcSplitLogit <- function(x, y, q.hat, FI, const.factor, n.iter, epsilon, L){

n.obs = dim(x)[1]	
n.param = dim(x)[2]	

post.samp <- matrix(NA, n.iter, n.param)

acpt.ind <- 1
current.q <- q.hat
current.U <- U(x, y, current.q, sigma)
current.g = grad.U1(x, y, current.q, sigma, q.hat, FI) 

post.samp[1, ] <- current.q
for (i in 2:n.iter){
	samp <- HMCsplit(x, y, current.q, current.U, current.g, sigma, R, q.hat, FI, const.factor, n.param, epsilon, L)
	current.q <- samp$q
	current.U <- samp$U
	current.g <- samp$g
	post.samp[i, ] <- current.q
	acpt.ind[i] <- samp$Ind
	
	}
return(list(samp=post.samp, acpt.ind=acpt.ind))
}



HMCsplit = function (x, y, current.q, current.U, current.g, sigma, R, q.hat, FI,  const.factor, n.param, e, L)
{	
	e <- runif(1, 0.95*e, 1.05*e)	
	
    q = current.q
    p = rnorm(n.param)	
    g = current.g
  
    current.p = p
    current.K = sum(current.p^2)/2
    current.H = current.U+current.K;
  


  for (i in 1:L)
  {
  	# Outer splitting
    p = p - e * g / 2 # The first half step for momentum with potential U1

    # Inner splitting
	#direct solution by diagonizing A
    X <-  R%*% c(q - q.hat,p)  # solution of dX/dt = A %*% X    
    q <- X[1:n.param]+q.hat
    p <- X[(n.param+1):(2*n.param)]  
    # End of inner spliting
    
    g = grad.U1(x, y, q, sigma, q.hat, FI) 
    p = p - e * g/ 2 # The last half step for momentum with potential U1
  }
  proposed.U = U(x, y, q, sigma)  
  proposed.K = sum(p^2)/2
  proposed.H = proposed.U + proposed.K

  acceptProb = min(1, exp(current.H - proposed.H))
  if (runif(1) < acceptProb)
  {
    return (list(q = q, U = proposed.U, g = g, Ind = 1))  # accept
  }
  else
  {
    return (list(q = current.q, U = current.U, g = current.g, Ind = 0))  # reject
  }
}



#### U

U = function(x, y, beta, sigma){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

E = -(logLike + logPrior)

}

grad.U = function(x, y, beta, sigma){

eta = x%*%beta;

dLogLike = t(x)%*%(y - exp(eta - log(1 + exp(eta)) ))
dLogPrior =  -(beta) / (sigma^2)
 
g = -(dLogLike + dLogPrior);


}


#### U0


U0 = function(beta, beta.hat, FI, const.factor, sigma){

E <- const.factor + t(beta - beta.hat)%*%FI%*%(beta - beta.hat) / 2 

}

grad.U0 = function(beta, beta.hat, FI, sigma){

g = t(beta - beta.hat)%*%FI

}




#### U1


U1 = function(x, y, beta, sigma, u0){

eta = x%*%beta
logLike =  sum(y*eta - log(1+exp(eta)))
logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

u = -(logLike + logPrior)

E = u - u0

}

grad.U1 = function(x, y, beta, sigma, beta.hat, FI){

g0 = t(t(beta - beta.hat)%*%FI)

eta = x%*%beta;

dLogLike = t(x)%*%(y - exp(eta - log(1 + exp(eta)) ))
dLogPrior =  -(beta) / (sigma^2)

g = -(dLogLike + dLogPrior) - g0


}



get.map = function(beta){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

E = -(logLike + logPrior)

}



ac.time <- function(x, q, m){

n = length(x)

s2 = var(x)

batch.mean = NULL
for(i in 1:q){
	
	batch.mean[i] <- mean(x[(((i-1)*m)+1):(i*m)])
	
	}	

	return(m*var(batch.mean)/s2)
	
}

n.param = dim(x)[2]

opt.res <- optim(rnorm(n.param, 0, .2), get.map, method = "BFGS", hessian=TRUE)

# Posterior mode
q.hat <- opt.res$par
FI <- opt.res$hessian

eta = x%*%q.hat
const.factor = - sum(y*eta - log(1+exp(eta))) # this is log(P.star(mode)) in Laplace's method

# To get R which is used for the inner loop of HMC
D = length(q.hat)
A <- matrix(0,2*D,2*D)
A[1:D,(D+1):(2*D)] <- diag(1,D)
A[(D+1):(2*D),1:D] <- -FI 
eig <- eigen(A)
lambda <- eig$values
Gamma <- eig$vectors
#and get its eigen-decomposition

R = Re(Gamma %*% diag(exp(epsilon *lambda)) %*% solve(Gamma))



start.time = proc.time()
res <- hmcSplitLogit(x, y, q.hat, FI, const.factor, nIter, epsilon, leapFrog)
end.time = proc.time()
cpu.time = end.time[1] - start.time[1]

samp = res$samp
save(samp, file='hmcSplitExactLogit.Rdata')

get.logLike = function(beta){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

}

acp = round(mean(res$acpt.ind), 2)
print(paste('Acceptance rate: ', acp))

CPU = round(cpu.time/nIter, 4)

print(paste('CPU time/iter', CPU))


logLike <- NULL
for(i in 1:dim(samp)[1]){

	logLike[i] <- get.logLike(samp[i, ])	
	
}


act.batch = NULL
batch.size = 1000
n.batch = floor (nIter/batch.size)

for(i in 1:dim(samp)[2]){
	
	act.batch[i] = ac.time(samp[, i], n.batch, batch.size)	
	
}
act = round(max(act.batch), 2)

print(paste('Batch autocorrelation time: ', act))

act.loglike = round(ac.time(logLike, n.batch, batch.size), 2)

print(paste('Batch autocorrelation time-- logLike: ', act.loglike))

print(paste('Batch autocorrelation time X s: ', act*CPU))

print(paste('Batch autocorrelation time X L: ', act*leapFrog))

print(paste('Batch autocorrelation time X acpRate: ', act*acp))
