rm(list=ls())

library(numDeriv)
library(norm)

set.seed(1)

makeData = function(){
data = read.table('chess.txt', header = FALSE, sep=',')

y = as.numeric(data[, 37]=='won')

X = data[, 1:36]
x = matrix(0, dim(X)[1], 36)
for(i in 1:36){

	x[, i] = as.numeric(X[, i])-1
}


x =  cbind(1, x)

save(x, y, file='chess.Rdata')
}

load('chess.Rdata')

nIter = 200000
burnIn = 1000

leapFrog = 9
epsilon= 0.08*20/9

M = 3

sigma = 10


hmcSplitLogit <- function(x, y, x0, y0, x1, y1, n.iter, epsilon, L, M){

n.param = dim(x)[2]	

post.samp <- matrix(NA, n.iter, n.param)

acpt.ind <- 1
current.q <- q.hat
post.samp[1, ] <- current.q

current.U = U(x, y, current.q, sigma) 

current.g = grad.U1(x1, y1, current.q, sigma)

for (i in 2:n.iter){
	samp <- HMCsplit(x, y, x0, y0, x1, y1, current.q, current.U, current.g, sigma, n.param, epsilon, L, M)
	current.q <- samp$q
	current.U <- samp$U
	current.g <- samp$g
	
	post.samp[i, ] <- current.q
	acpt.ind[i] <- samp$Ind
	
	}
return(list(samp=post.samp, acpt.ind=acpt.ind))
}



HMCsplit = function (x, y, x0, y0, x1, y1, current.q, current.U, current.g, sigma, n.param, e, L, M)
{

  e <- runif(1, 0.95*e, 1.05*e)		
	
  q = current.q	
  g = current.g 
  p = rnorm(n.param)

  current.p = p
  
   
  current.K = sum(current.p^2)/2
  current.H = current.U+current.K
  
  newG1 = g
  for (i in 1:L)
  {
  	# Outer splitting

    p = p - e * newG1 / 2 # The first half step for momentum with potential U1

    # Inner splitting
    newG0 = grad.U0(x0, y0, q, sigma)
    for (m in 1:M){ # update with potential U0

    	p = p - e / (2 * M) * newG0
    	q = q + (e / M) * p
    	newG0 = grad.U0(x0, y0, q, sigma)
    	p = p - e / (2 * M) * newG0
    }

	newG1 = grad.U1(x1, y1, q, sigma)
    p = p - e * newG1 / 2 # The last half step for momentum with potential U1
  }

  # Evaluate potential and kinetic energies at start and end of trajectory

proposed.U = U(x, y, q, sigma) 

proposed.K = sum(p^2)/2
proposed.H = proposed.U + proposed.K

  acceptProb = min(1, exp(current.H - proposed.H))
  if (runif(1) < acceptProb)
  {
    return (list(q = q, U = proposed.U, g = newG1, Ind = 1))  # accept

  }
  else
  {
    return (list(q = current.q, U = current.U, g = current.g, Ind = 0))  # reject

  }
}



#### U

U = function(x, y, beta, sigma){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

E = -(logLike + logPrior)

}

grad.U = function(x, y, beta, sigma){

eta = x%*%beta;

dLogLike = t(x)%*%(y - exp(eta - log(1 + exp(eta)) ))
dLogPrior =  -(beta) / (sigma^2)
 
g = -(dLogLike + dLogPrior);


}


#### U0


U0 = function(x, y, beta, sigma){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

E = -(logLike)

}

grad.U0 = function(x, y, beta, sigma){

eta = x%*%beta;

dLogLike = t(x)%*%(y - exp(eta - log(1 + exp(eta)) ))
 
g = -(dLogLike)

}




#### U1


U1 = function(x, y, beta, sigma){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

E = -(logLike + logPrior)

}

grad.U1 = function(x, y, beta, sigma){


eta = x%*%beta;

dLogLike = t(x)%*%(y - exp(eta - log(1 + exp(eta)) ))
dLogPrior =  -(beta) / (sigma^2)
 
g = -(dLogLike + dLogPrior);

}




get.map = function(beta){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

logPrior =  sum( (-0.5*(beta)^2 ) / (sigma^2) )

E = -(logLike + logPrior)

}


#########

ac.time <- function(x, q, m){

n = length(x)

s2 = var(x)

batch.mean = NULL
for(i in 1:q){
	
	batch.mean[i] <- mean(x[(((i-1)*m)+1):(i*m)])
	
	}	

	return(m*var(batch.mean)/s2)
	
}



n.param = dim(x)[2]

q.hat <- optim(rnorm(n.param, 0, .2), get.map, method = "BFGS")$par

p =  (exp(x%*%q.hat)/(1+exp(x%*%q.hat)))
y.hat <- round(p)
p = cbind(p, 1- p)
#U0.ind = which(y.hat < 0.55 & y.hat > 0.45)

ent <- -rowSums(p*log(p))
ent[is.na(ent)] <- 0
ind1 = which(ent > quantile(ent, 0.7))
U0.ind = ind1

U1.ind = setdiff(seq(1, dim(x)[1]), U0.ind)


x0 = x[U0.ind, ]
x1 = x[U1.ind, ]

y0 = y[U0.ind]
y1 = y[U1.ind]



#M1 <- glm(y ~ X, family=binomial(link=logit))

#M1 <- lm(y ~ X)	
#q.hat <- as.matrix(coef(M1))


start.time = proc.time()
res <- hmcSplitLogit(x, y, x0, y0, x1, y1, nIter, epsilon, leapFrog, M)
end.time = proc.time()
cpu.time = end.time[1] - start.time[1]


samp = res$samp
save(samp, file='hmcSplitDataLogit.Rdata')

get.logLike = function(beta){

eta = x%*%beta

logLike =  sum(y*eta - log(1+exp(eta)))

}

acp = round(mean(res$acpt.ind), 2)
print(paste('Acceptance rate: ', acp))

CPU = round(cpu.time/nIter, 4)

print(paste('CPU time/iter', CPU))


logLike <- NULL
for(i in 1:dim(samp)[1]){

	logLike[i] <- get.logLike(samp[i, ])	
	
}


act.batch = NULL
batch.size = 1000
n.batch = floor (nIter/batch.size)

for(i in 1:dim(samp)[2]){
	
	act.batch[i] = ac.time(samp[, i], n.batch, batch.size)	
	
}
act = round(max(act.batch), 2)

print(paste('Batch autocorrelation time: ', act))

act.loglike = round(ac.time(logLike, n.batch, batch.size), 2)

print(paste('Batch autocorrelation time-- logLike: ', act.loglike))

print(paste('Batch autocorrelation time X s: ', act*CPU))

print(paste('Batch autocorrelation time X L: ', act*leapFrog))

print(paste('Batch autocorrelation time X acpRate: ', act*acp))
